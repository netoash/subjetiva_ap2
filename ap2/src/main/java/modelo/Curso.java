package modelo;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_curso")
public class Curso {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="cd_curso")
	private Long id;
	@Column(name="ds_nome")
	private String nome;
	@Column(name="ds_qtdSemestres")
	private Integer qtd_semestres;
	@Column(name="ds_tipo")
	private String tipo;
	@ManyToMany(mappedBy = "cursos", cascade = CascadeType.ALL)
	private List<Aluno> alunos;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getQtd_semestres() {
		return qtd_semestres;
	}
	public void setQtd_semestres(Integer qtd_semestres) {
		this.qtd_semestres = qtd_semestres;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public List<Aluno> getAlunos() {
		return alunos;
	}
	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

}
